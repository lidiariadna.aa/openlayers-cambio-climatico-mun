import './style.css';
import {Map, View} from 'ol';
import KML from 'ol/format/KML.js';
import StadiaMaps from 'ol/source/StadiaMaps.js';
import VectorSource from 'ol/source/Vector.js';
import {Fill, Stroke, Style} from 'ol/style.js';
import {Tile as TileLayer, Vector as VectorLayer} from 'ol/layer.js';

const styleFunction = function(feature) {
  return new Style({
    fill: new Fill({
      color: 'rgba(139, 0, 0, 0.6)', 
    }),
    stroke: new Stroke({
      color: '#FB9902', 
      width: 1, 
    }),
    // 
  });
};

const vector = new VectorLayer({
  source: new VectorSource({
    url: 'mun_vulnerables.kml',
    format: new KML({
      extractStyles: false,
    }),
  }),
  style: styleFunction,
});

const raster = new TileLayer({
  source: new StadiaMaps({
    layer: 'alidade_satellite',
  }),
});

const map = new Map({
  target: 'map',
  layers: [raster, vector],
  view: new View({
    center: [-99.12766, 19.42847],
    projection: "EPSG:4326",
    zoom: 5
  })
});


